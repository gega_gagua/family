import Layout from '../../layout'
import Back from '../../components/Back'
import LoginHeader from '../../components/LoginHeader'

export default function Login() {  
  return (
    <Layout>
        <div className="login">
            <Back />
            <LoginHeader />
        </div>
    </Layout>
  )
}

