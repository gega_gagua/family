import Layout from '../layout'
import Link from "next/link";
import Router, { useRouter } from "next/router";
// import styles from "../css/Home.module.css";

export default function Home() {
  const router = useRouter();

  return (
    <Layout>
      <div className='home'>
        <div className='home__skip'>
          <span>SKIP</span>
        </div>

        <div className="home__logo">
          <img src="./img/home.png"/>
        </div>

        <div className='slider'>
          <div className='slides'>
            <div name="slide-1" id="slide-1">
              <h3>La cocina de Dani García en tu casa</h3>
              <p>Amet vitae egestas pellentesque pretium ac. Donec pellentesque ornare nisi.</p>
            </div>
            <div name="slide-2" id="slide-2">
              <h3>Diferentes tipos de comida en un mismo pedido</h3>
              <p>Todo lo que pidas de nuestras cartas en un mismo pedido</p>
            </div>
            <div id="slide-3">
              <h3>Haz pedidos compartidos con amigos</h3>
              <p>Invita a tus amigos o famliia a que compartan su cesta</p>
            </div>
          </div>

          <div className='dots'>
            <li className={router.asPath == '/#slide-1' || router.asPath == '/' ? 'active' : ''}>
              <Link 
                href="#slide-1" 
                scroll={false}>
                  1
              </Link>
            </li>
            <li className={router.asPath == '/#slide-2' ? 'active' : ''} >
              <Link 
                href="#slide-2" 
                scroll={false}>
                  2
              </Link>
            </li>
            <li className={router.asPath == '/#slide-3' ? 'active' : ''} >
              <Link 
                href="#slide-3" 
                scroll={false}>
                  3
              </Link>
            </li>

            <span>
              Siguiente
            </span>
          </div>
        </div>
      </div>
    </Layout>
  )
}
