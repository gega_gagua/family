export default function LoginHeader() {
  
  return (
    <div className="login-header">
        <div className="login-header__img">
            <img src="./img/login.svg" />
        </div>
        <div className="login-header__text">
            <h4>
                Bienvenido a La Gran Familia Mediterranea
            </h4>
            <ul>
                <li>
                    Pedidos mixtos de cualquier categoría 
                </li>
                <li>
                    Crea cestas compartidas
                </li>
                <li>
                    Loren ipsum...
                </li>
            </ul>
        </div>
    </div>
  )
}
