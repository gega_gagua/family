import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function Back() {
  return (
    <div className="back">
        <span>
          <FontAwesomeIcon size="lg" icon={faArrowLeft} />
        </span>
        Iniciar sesión
    </div>
  )
}