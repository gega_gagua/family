import Head from 'next/head'

export default function Layout({ children }) {
  return (
    <>
      <Head>
          <title>Create Next App</title>
          <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <div className="container">
          {children}
        </div>
      </main>
    </>
  )
}